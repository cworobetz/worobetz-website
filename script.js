Vue.component('link-item', {
  props: ['link'],
  template: '<li v-bind:onclick="link.onclick"><a v-bind:href="link.destination">{{ link.text }}</a></li>'
})

Vue.component('section-item', {
  props: ['section'],
  template: '<div v-bind:id="section.header" class="section"><a v-bind:name="section.header"></a><h1>{{ section.header }}</h1><div v-html="section.content"></div></div>'
})

var header = new Vue({
  el: '#header',

  data: {
    headerLinks: [
      { id: 1, text: 'Git', destination: "https://gitlab.com/cworobetz"},
      { id: 2, text: 'Blog', destination: "https://blog.worobetz.ca"},
      { id: 3, text: 'Resume', destination: "https://drive.google.com/file/d/1zGwnj07hflfcStONBrFYb8S4IREl4-Yc/view?usp=sharing"}
    ]
  }
});

var content = new Vue({
  el: '#content',

  data: {
    sections: [
      { id: 0, header: 'About', content: "My name is Cooper Worobetz. I live in Vancouver, British Columbia." },
      { id: 1, header: 'Contact', content: "Email: cooper@worobetz.ca" }
    ]
  }
});
