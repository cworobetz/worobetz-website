Worobetz Website
================

Master / Live @ https://worobetz.ca:

[![pipeline status](https://gitlab.com/cworobetz/worobetz-website/badges/master/pipeline.svg)](https://gitlab.com/cworobetz/worobetz-website/commits/master)

Develop @ https://develop.worobetz.ca:

[![pipeline status](https://gitlab.com/cworobetz/worobetz-website/badges/master/pipeline.svg)](https://gitlab.com/cworobetz/worobetz-website/commits/develop)

This repository contains necessary HTML, CSS, PHP, Shell, and JS files to run
my personal website hosted at https://worobetz.ca

I use my website / this repository to test out new technology and to learn new concepts.

This repository makes use of a lot of GitLab's features, including Gitlab CI and GitLab's Container registry
 
On each commit, docker images for nginx and php are built in
`.gitlab-ci.yml`'s build stage with the code baked in. These are pushed to
GitLab's container registry, for use in the deploy stage. 

There's several deployment options. This website started out using a hacky (yet
fun and self aware!) bash script that would pull from git. Later, it was converted to deploying
using GitLab + docker-compose, then Gitlab + Kubernetes, then back to GitLab +
docker-compose, and now to its current iteration, which is purely GitLab CI + GitLab
pages. This reduces my hosting costs to zero while still allowing for a nice
CI/CD setup and having a custom, HTTPS-secured domain.
